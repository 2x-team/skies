uniform sampler2D texture0;
uniform sampler2D texture1;
varying vec2 v_texCoord;
varying vec3 v_light;
varying vec3 v_view;
varying vec4 v_ambient;

void main( void ) {
  vec3 normal = normalize(texture2D(texture1, v_texCoord).xyz - 0.5);

  //vec4 ambient = gl_FrontLightModelProduct.sceneColor;
  vec4 diffuse = gl_FrontLightProduct[0].diffuse * max(0.0, dot(v_light, normal));
  vec4 specular = gl_FrontLightProduct[0].specular * pow(dot(reflect(v_light, normal), normalize(v_view)), gl_FrontMaterial.shininess);

  gl_FragColor = (v_ambient + diffuse) * gl_Color * texture2D(texture0, v_texCoord) + specular;
}

