uniform mat4 bones[8];
varying vec2 texCoord;
varying vec4 ambientdiffuse;

void main(void) {

  vec4 AnimVertex = (bones[0] * gl_Vertex) * gl_MultiTexCoord2.x
                  + (bones[1] * gl_Vertex) * gl_MultiTexCoord2.y
                  + (bones[2] * gl_Vertex) * gl_MultiTexCoord2.z
                  + (bones[3] * gl_Vertex) * gl_MultiTexCoord2.w
                  + (bones[4] * gl_Vertex) * gl_MultiTexCoord3.x
                  + (bones[5] * gl_Vertex) * gl_MultiTexCoord3.y
                  + (bones[6] * gl_Vertex) * gl_MultiTexCoord3.z
                  + (bones[7] * gl_Vertex) * gl_MultiTexCoord3.w;

  vec3 AnimNormal = normalize((mat3(bones[0][0].xyz, bones[0][1].xyz, bones[0][2].xyz) * gl_Normal) * gl_MultiTexCoord2.x
                            + (mat3(bones[1][0].xyz, bones[1][1].xyz, bones[1][2].xyz) * gl_Normal) * gl_MultiTexCoord2.y
                            + (mat3(bones[2][0].xyz, bones[2][1].xyz, bones[2][2].xyz) * gl_Normal) * gl_MultiTexCoord2.z
                            + (mat3(bones[3][0].xyz, bones[3][1].xyz, bones[3][2].xyz) * gl_Normal) * gl_MultiTexCoord2.w
                            + (mat3(bones[4][0].xyz, bones[4][1].xyz, bones[4][2].xyz) * gl_Normal) * gl_MultiTexCoord3.x
                            + (mat3(bones[5][0].xyz, bones[5][1].xyz, bones[5][2].xyz) * gl_Normal) * gl_MultiTexCoord3.y
                            + (mat3(bones[6][0].xyz, bones[6][1].xyz, bones[6][2].xyz) * gl_Normal) * gl_MultiTexCoord3.z
                            + (mat3(bones[7][0].xyz, bones[7][1].xyz, bones[7][2].xyz) * gl_Normal) * gl_MultiTexCoord3.w);


  vec3 pos = vec3(gl_ModelViewMatrix * AnimVertex);
  vec3 light = normalize(gl_LightSource[0].position.xyz - pos);

  vec3 normal = gl_NormalMatrix * AnimNormal;
  gl_Position = gl_ModelViewProjectionMatrix * AnimVertex;
  texCoord = gl_MultiTexCoord0.xy;

  ambientdiffuse = gl_FrontMaterial.emission + gl_LightModel.ambient * gl_FrontMaterial.ambient + gl_FrontLightProduct[0].ambient + max(0.0, dot(normal, light)) * gl_FrontLightProduct[0].diffuse;
  ambientdiffuse.a = 1.0;

  gl_FrontColor = gl_Color;
}
