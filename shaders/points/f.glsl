uniform sampler2D texture0;
//varying float fog;
//varying vec4 ambientdiffuse;

void main (void) {
  vec4 color = /*ambientdiffuse **/ gl_Color * texture2D(texture0, gl_TexCoord[0].xy);
  gl_FragColor = color;//mix(color, gl_Fog.color, fog);
}

