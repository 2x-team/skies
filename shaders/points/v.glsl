//varying float fog;
//varying vec4 ambientdiffuse;

void main(void) {
  vec3 v = vec3(gl_ModelViewMatrix * gl_Vertex);
  gl_Position = gl_ModelViewProjectionMatrix * gl_Vertex;
  float d = -v.z;
  gl_PointSize = gl_MultiTexCoord0.x * gl_Point.size * sqrt(1.0 / (gl_Point.distanceConstantAttenuation + gl_Point.distanceLinearAttenuation*d + gl_Point.distanceQuadraticAttenuation*d*d));

  //ambientdiffuse = gl_FrontMaterial.emission + gl_LightModel.ambient * gl_FrontMaterial.ambient + gl_FrontLightProduct[0].ambient + gl_FrontLightProduct[0].diffuse;
  //ambientdiffuse.a = 1.0;

  gl_FrontColor = gl_Color;
  //fog = max(0.0, -v.z * gl_Fog.scale - gl_Fog.start);
}
