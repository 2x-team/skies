//uniform mat4 matrix;
//uniform vec3 eyepos;
varying vec3 normal;
varying vec3 pos;
varying vec2 texCoord;
varying vec3 light;
varying float fog;
varying vec4 ambientdiffuse;

void main(void) {
  vec3 v = vec3(gl_ModelViewMatrix * gl_Vertex);
  light = normalize(gl_LightSource[0].position.xyz - v);
  pos = normalize(v);

  normal = gl_NormalMatrix * gl_Normal;
  gl_Position = gl_ModelViewProjectionMatrix * gl_Vertex;
  texCoord = gl_MultiTexCoord0.xy;

  ambientdiffuse = gl_FrontMaterial.emission + gl_LightModel.ambient * gl_FrontMaterial.ambient + gl_FrontLightProduct[0].ambient + max(0.0, dot(normal, light)) * gl_FrontLightProduct[0].diffuse;
  ambientdiffuse.a = 1.0;

  gl_FrontColor = gl_Color;
  //vec4 wpos = matrix * gl_Vertex;
  fog = max(0.0, -v.z * gl_Fog.scale - gl_Fog.start);
  //fog = (100.0 - min(wpos.y, eyepos.y)) / abs(eyepos.y-wpos.y) * -v.z * gl_Fog.scale;
}
