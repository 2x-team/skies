uniform mat4 bones[5];
varying vec3 normal;
varying vec3 pos;
varying vec2 texCoord;
varying vec3 light;
varying vec4 ambientdiffuse;

void main(void) {
  /*int bone0 = int(gl_MultiTexCoord1.x + 0.5);
  int bone1 = int(fract(gl_MultiTexCoord1.x) * 256.0 + 0.5);
  int bone2 = int(gl_MultiTexCoord1.y + 0.5);

  float weight0 = gl_MultiTexCoord1.z;
  float weight1 = gl_MultiTexCoord1.w;
  float weight2 = 1.0 - weight0 - weight1;*/

  vec4 AnimVertex = (bones[0] * gl_Vertex) * gl_MultiTexCoord2.x
                  + (bones[1] * gl_Vertex) * gl_MultiTexCoord2.y
                  + (bones[2] * gl_Vertex) * gl_MultiTexCoord2.z
                  + (bones[3] * gl_Vertex) * gl_MultiTexCoord2.w
                  + (bones[4] * gl_Vertex) * (1.0 - gl_MultiTexCoord2.x - gl_MultiTexCoord2.y - gl_MultiTexCoord2.z - gl_MultiTexCoord2.w);

  /*mat3 normalM0 = mat3(bones[0][0].xyz, bones[0][1].xyz, bones[0][2].xyz);
  mat3 normalM1 = mat3(bones[1][0].xyz, bones[1][1].xyz, bones[1][2].xyz);
  mat3 normalM2 = mat3(bones[2][0].xyz, bones[2][1].xyz, bones[2][2].xyz);
  mat3 normalM3 = mat3(bones[3][0].xyz, bones[3][1].xyz, bones[3][2].xyz);
  mat3 normalM4 = mat3(bones[4][0].xyz, bones[4][1].xyz, bones[4][2].xyz);*/

  vec3 AnimNormal = normalize((mat3(bones[0][0].xyz, bones[0][1].xyz, bones[0][2].xyz) * gl_Normal) * gl_MultiTexCoord2.x
                            + (mat3(bones[1][0].xyz, bones[1][1].xyz, bones[1][2].xyz) * gl_Normal) * gl_MultiTexCoord2.y
                            + (mat3(bones[2][0].xyz, bones[2][1].xyz, bones[2][2].xyz) * gl_Normal) * gl_MultiTexCoord2.z
                            + (mat3(bones[3][0].xyz, bones[3][1].xyz, bones[3][2].xyz) * gl_Normal) * gl_MultiTexCoord2.w
                            + (mat3(bones[4][0].xyz, bones[4][1].xyz, bones[4][2].xyz) * gl_Normal) * (1.0 - gl_MultiTexCoord2.x - gl_MultiTexCoord2.y - gl_MultiTexCoord2.z - gl_MultiTexCoord2.w));

  /*vec3 AnimNormal = normalize((boneOrients[0] * gl_Normal) * gl_MultiTexCoord2.x
                            + (boneOrients[1] * gl_Normal) * gl_MultiTexCoord2.y
                            + (boneOrients[2] * gl_Normal) * gl_MultiTexCoord2.z
                            + (boneOrients[3] * gl_Normal) * gl_MultiTexCoord2.z
                            + (boneOrients[4] * gl_Normal) * (1.0 - gl_MultiTexCoord2.x - gl_MultiTexCoord2.y - gl_MultiTexCoord2.z - gl_MultiTexCoord2.w));*/

  pos = vec3(gl_ModelViewMatrix * AnimVertex);
  light = normalize(gl_LightSource[0].position.xyz - pos);
  pos = normalize(pos);

  normal = gl_NormalMatrix * AnimNormal;
  gl_Position = gl_ModelViewProjectionMatrix * AnimVertex;
  texCoord = gl_MultiTexCoord0.xy;

  ambientdiffuse = gl_FrontMaterial.emission + gl_LightModel.ambient * gl_FrontMaterial.ambient + gl_FrontLightProduct[0].ambient + max(0.0, dot(normal, light)) * gl_FrontLightProduct[0].diffuse;
  ambientdiffuse.a = 1.0;

  gl_FrontColor = gl_Color;
}
