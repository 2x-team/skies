uniform sampler2D texture0;
varying vec2 texCoord;
varying vec3 normal;
varying vec3 pos;
varying vec3 light;
varying float fog;
varying vec4 ambientdiffuse;

void main (void) {
  vec4 specular = gl_FrontLightProduct[0].specular * pow(dot(reflect(light, normal), pos), gl_FrontMaterial.shininess);

  vec4 color = ambientdiffuse * gl_Color * texture2D(texture0, texCoord) + specular;
  gl_FragColor = mix(color, gl_Fog.color, fog);
}

