varying vec2 v_texCoord;
varying vec3 v_light;
varying vec3 v_view;
varying float fog;
varying vec4 v_ambient;

void main(void) {
  mat3 matrix;
  matrix[0] = gl_NormalMatrix * gl_MultiTexCoord1.xyz;
  matrix[1] = gl_NormalMatrix * cross(gl_Normal, gl_MultiTexCoord1.xyz);
  matrix[2] = gl_NormalMatrix * gl_Normal;

  vec3 pos = (gl_ModelViewMatrix * gl_Vertex).xyz;
  v_light = normalize((gl_LightSource[0].position.xyz - pos) * matrix);
  v_view = pos * matrix;

  gl_Position = gl_ModelViewProjectionMatrix * gl_Vertex;
  v_texCoord = gl_MultiTexCoord0.xy;
  v_ambient = gl_FrontLightProduct[0].ambient + gl_FrontMaterial.emission + gl_LightModel.ambient * gl_FrontMaterial.ambient;
  gl_FrontColor = gl_Color;
  fog = max(0.0, -pos.z * gl_Fog.scale - gl_Fog.start);
}

