uniform sampler2D texture0;
uniform sampler2D texture1;
varying vec2 texCoord;
varying vec3 normal;
varying vec3 pos;
varying vec3 light;
varying vec4 ambientdiffuse;

void main (void) {
  vec4 specular = gl_FrontLightProduct[0].specular * texture2D(texture1, texCoord) * pow(dot(reflect(light, normal), pos), gl_FrontMaterial.shininess);

  gl_FragColor = ambientdiffuse * gl_Color * texture2D(texture0, texCoord) + specular;
}

