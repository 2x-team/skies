uniform sampler2D texture0;
varying vec2 texCoord;
varying vec4 ambientdiffuse;

void main (void) {
  gl_FragColor = ambientdiffuse * gl_Color * texture2D(texture0, texCoord);
}

