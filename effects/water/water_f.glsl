uniform sampler2D texture0;
uniform sampler2D texture1;
uniform sampler2D texture2;
varying vec2 v_normalCoord;
varying vec2 v_specularCoord;
varying vec2 v_texCoord;
varying vec3 v_light;
varying vec3 v_view;
varying float fog;
varying vec4 v_ambient;

void main( void ) {
  vec3 normal = normalize(texture2D(texture1, v_normalCoord).xyz - 0.5);

  //vec4 ambient = gl_FrontLightModelProduct.sceneColor;
  vec4 diffuse = gl_FrontLightProduct[0].diffuse * max(0.0, dot(v_light, normal));
  vec4 specular = gl_FrontLightProduct[0].specular * texture2D(texture2, v_specularCoord) * pow(dot(reflect(v_light, normal), normalize(v_view)), gl_FrontMaterial.shininess);

  vec4 color = (v_ambient + diffuse) * gl_Color * texture2D(texture0, v_texCoord) + specular;
  gl_FragColor = mix(color, gl_Fog.color, fog);
}

