Skies beta - � Aukiogames
http://aukiogames.com/skies

To change configuration options edit userdata/settings.script with notepad.

Credits

PROJECT LEAD - Sami Kalliokoski (ceesam)

TECHNICAL DIRECTOR / CODING - Antti Lehto (Gebbiz)
 
ARTISTS
 Level design - Peter Finnberg (stormi)
 Vehicles / objects - Mika G��s (Migugi)
 Characters - Sami Kalliokoski (ceesam)
 Objects - Mikko Tyni (mty)
 Concepts - Juuso M�kel� (maknien)

AUDIO - Joonas Kaski (Jonezky)

ADDITIONAL WORK
 Additional coding - Simo Salminen (ranq)
 Additional design - Arttu Pylkk� (rudecki)