$window = GLOBALS["window"];
$game = GLOBALS["game"];
$settings = GLOBALS["settings"];
$viewport = GLOBALS["viewport"];
$desktop = GLOBALS["desktop"];
$player = null;

$cameras = [];
$current = null;
$curindex = 0;

with ($cameras[] = new_floatingCamera($game)) {
  .setPosition(0.0, 3.0, 8.5);
  .setOrientation(0.0, 1.0, 0.0, 0.0);
  .fov = $settings.fov ? 60.0;
  .distance = 4000.0;
  .smoothness = 0.002;
  .lockVertical = false;
  .enabled = false;
}

with ($cameras[] = new_floatingCamera($game)) {
  .setPosition(0.0, 4.5, 15.0);
  .setOrientation(0.0, 1.0, 0.0, 0.0);
  .fov = $settings.fov ? 60.0;
  .distance = 4000.0;
  .smoothness = 0.005;
  .lockVertical = true;
  .enabled = false;
}

with ($cameras[] = new_fixedCamera($game)) {
  .setPivot(2.0, 0.3, -0.5);
  .setPosition(0.0, 0.0, 1.0);
  .setOrientation(0.0, 1.0, 0.0, 0.0);
  .fov = $settings.fov ? 60.0;
  .distance = 4000.0;
  .enabled = false;
}

$lockOrient = false;
setCurrent($cameras[0]);

function setCurrent($camera) {
  if ($current && ($current != $camera)) {
    $camera.target = $current.target;
    $current.target = null;
    $current.enabled = false;
  }
  $current = $camera;
  $viewport.camera = $camera;
  GLOBALS["camera"] = $camera;
  $camera.enabled = true;
  $camera.lockOrient = $lockOrient;
  $camera.reset();
}

event $game:playerLoad($airplane) {
  $current.target = $airplane;
  $current.lockOrient = $lockOrient = false;
  setCurrent($current);
  $player = GLOBALS["player"];
}

event $game:player2Load($airplane) {
  $current.target = $airplane.script.gunnerMain;
  $current.lockOrient = $lockOrient = false;
  setCurrent($current);
  $player = GLOBALS["player"];
}

event $game:playerUnload() {
  $current.target = null;
  $player = null;
}

event $game:player2Unload() {
  $current.target = null;
  $player = null;
}

event $game:playerDamage($airplane) {
  if ($airplane.script.damage_count >= 5)
    $current.lockOrient = $lockOrient = true;
}

event $game:playerReseted() {
  $current.lockOrient = $lockOrient = false;
  $current.reset();
}

with ($msgCtrl = new_control($desktop)) {
  .setFont("fonts/verdana", YELLOW, 0.020, 0.020);
  .setTextShadow(color(BLACK, 96), 0.002, 0.002);
  .setRect(0.375, 0.100, 0.250, 0.025);
  .text = "";
}
$timer = null;

function showMessage($msg) {
  $msgCtrl.text = $msg;
  $msgCtrl.blend(1.0, 0.1);
  if ($timer) {
    $timer.restart();
    return;
  }

  $timer = timer(2.0);
  event $timer:timeout {
    $msgCtrl.blend(0.0, 0.3);
    $timer = NULL;
    return false;
  }
}

event $window:{"keyDown_"+$settings.keyDecreaseCameraZ ? "MouseWheelUp"} {
  if ($window.inputTarget != $player) return;
  with ($current) {
    .z *= 0.95;
    showMessage("Camera distance: "+.z+"m");
  }
}

event $window:{"keyDown_"+$settings.keyIncreaseCameraZ ? "MouseWheelDown"} {
  if ($window.inputTarget != $player) return;
  with ($current) {
    .z /= 0.95;
    showMessage("Camera distance: "+.z+"m");
  }
}

event $window:{"keyDown_"+$settings.keyLookBack ? "B"} {
  if ($window.inputTarget != $player) return;
  $current.lookback = true;
  //showMessage("Camera direction: Backward");
}

event $window:{"keyUp_"+$settings.keyLookBack ? "B"} {
  if ($window.inputTarget != $player) return;
  $current.lookback = false;
  //showMessage("Camera direction: Forward");
}

$cam_angle = 0.0;

event $window:{"keyDown_"+$settings.keyLookLeft ? "Q"} {
  if ($window.inputTarget != $player) return;
  if ($cam_angle >= 0.0) $cam_angle -= 1.57;
  $current.setOrientation($cam_angle, 0.0, 1.0, 0.0);
}

event $window:{"keyUp_"+$settings.keyLookLeft ? "Q"} {
  if ($window.inputTarget != $player) return;
  if ($cam_angle <= 0.0) $cam_angle += 1.57;
  $current.setOrientation($cam_angle, 0.0, 1.0, 0.0);
}

event $window:{"keyDown_"+$settings.keyLookRight ? "E"} {
  if ($window.inputTarget != $player) return;
  if ($cam_angle <= 0.0) $cam_angle += 1.57;
  $current.setOrientation($cam_angle, 0.0, 1.0, 0.0);
}

event $window:{"keyUp_"+$settings.keyLookRight ? "E"} {
  if ($window.inputTarget != $player) return;
  if ($cam_angle >= 0.0) $cam_angle -= 1.57;
  $current.setOrientation($cam_angle, 0.0, 1.0, 0.0);
}

event $window:{"keyDown_"+$settings.keyChangeCamera ? "C"} {
  if ($window.inputTarget != $player) return;
  ++$curindex;
  if ($curindex >= $cameras.count) $curindex = 0;
  setCurrent($cameras[$curindex]);
  showMessage("Selected camera: "+($curindex+1));
}

with ($spectext = new_control($desktop)) {
  .setRect($desktop.width*0.5 - 0.125, 0.010, 0.250, 0.050);
  .setFont("fonts/verdana", WHITE, 0.025, 0.025);
  .setTextShadow(color(BLACK, 96), 0.002, 0.002);
  .text = "spectator mode";
}

event $game:setMode($spec) {
  if ($spec) {
    $spectext.blend(1.0, 0.5);
  } else {
    $spectext.blend(0.0, 0.5);
  }
}

destructor {
  delete $spectext;
  delete $msgCtrl;
  foreach ($cameras)
    delete $_;
  $viewport.camera = null;
  GLOBALS["camera"] = null;
}

